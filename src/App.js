import './App.css';
import Wrapper from './components/Wrapper'
require('./components/hoc')
function App() {
  return (
    <div className="App">
      <Wrapper />
    </div>
  );
}

export default App;
