import React from 'react'

const Rectangle = (props) =>(
        <div style={{width:props.width,height:props.height}} className="rectangle">
            {props.children}
        </div>
    )

export default Rectangle
