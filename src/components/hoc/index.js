import ComponentsGroup from '../componentsGroup/'
import Rectangle from '../Rectangle'
import withCustomBall from './withCustomBall'
ComponentsGroup.Rectangle = withCustomBall(props => <Rectangle {...props} />)