import React from 'react'

const CustomBall = (props) =>(
        <div style={{width:props.width,height:props.height,borderRadius:"100%"}} className="rectangle">
            {props.children}
        </div>
    )
export default CustomBall
