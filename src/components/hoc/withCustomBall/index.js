import CustomBall from './CustomBall'

const withCustomBall = Component => props =>{
    return(
        <div>
            {props.width === props.height 
            ? <CustomBall {...props}/>
            : <Component {...props}/>
            }
        </div>
    )
}
export default withCustomBall